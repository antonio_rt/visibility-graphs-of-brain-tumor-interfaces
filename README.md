# Visibility Graphs of Brain Tumor Interfaces #

![glioblastoma.PNG](https://bitbucket.org/repo/gXR68a/images/2878427517-glioblastoma.PNG)

![graph-spring-embedding2.PNG](https://bitbucket.org/repo/gXR68a/images/3646883915-graph-spring-embedding2.PNG)

Mathematica code to produce visibility graphs in **"Tumor Growth in the Brain: Complexity and Fractality"** by Miguel Martín-Landrove, Antonio Brú, Antonio Rueda-Toicen & Francisco Torres-Hoyos, to appear in Springer's "The Fractal Geometry of the Brain", editor: Antonio Di Ieva.

We propose the use of a solution of the Travelling Salesman Problem to define an ordering of walks from the center of mass of the tumor. This ordering is used to produce a synthetic "time" series from which the Hurst exponent can be calculated. The visibility graph of the tumor's interface is also produced considering this ordering.

###Repository owner: Antonio Rueda-Toicen
**antonio "dot" rueda "dot" toicen [ at ] gmail "dot" com**